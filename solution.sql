--1.
SELECT customerName FROM customers WHERE country = "Philippines";

--2.
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

--3.
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

--4.
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

--5.
SELECT customerName from customers WHERE state IS NULL;

--6.
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

--7.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

--8.
SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

--9.
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

--10.
SELECT productLine FROM productLines WHERE textDescription LIKE "%state of the art%";

--11.
SELECT DISTINCT country FROM customers;

--12.
SELECT DISTINCT status FROM orders;

--13.
SELECT customerName, country FROM customers WHERE  country = "USA" OR country = "France" OR country = "Canada";

--14.
SELECT firstName, lastName, city FROM employees
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE city = "Tokyo";

--15.
SELECT customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE lastName = "Thompson" AND firstName = "Leslie" AND employeeNumber = 1166;

--16.
SELECT productName, customerName FROM orders
	JOIN customers ON orders.customerNumber = customers.customerNumber
	JOIN orderDetails ON orders.orderNumber = orderDetails.orderNumber;
	JOIN products ON orderDetails.productCode = products.productCode
	WHERE customerName = "Baane Mini Imports";

--17.
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode

--18.
SELECT lastName, firstName FROM employees WHERE reportsTo = (
	SELECT employeeNumber FROM employees WHERE firstName = "Anthony" AND lastName = "Bow"
);

--19.
SELECT productName, MSRP FROM products WHERE MSRP = (SELECT MAX(MSRP) FROM products);

--20.
SELECT COUNT(customerNumber) FROM customers WHERE country = "UK";

--21.
SELECT productLine, COUNT(productCode) FROM products GROUP BY productLine;

--22.
SELECT employees.lastName, employees.firstName, (
	SELECT COUNT(customerNumber) FROM customers WHERE salesRepEmployeeNumber = employeeNumber
) noOfCustomers FROM employees;

--23.
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;
